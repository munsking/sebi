<?PHP
$p = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$g = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
if (isset($p, $p["reset"])) {
    unset($_SESSION["file"]);
    session_abort();
    session_destroy();
} else {
    session_start();
}
if (isset($_FILES["csv"]["tmp_name"])) {
    copy($_FILES["csv"]["tmp_name"], "/tmp/" . $_FILES["csv"]["name"]);
    $_SESSION["file"] = "/tmp/" . $_FILES["csv"]["name"];
}
$lineArr = file($_SESSION["file"]);
if ($lineArr) {
    ?>
    <html>
        <head>

        </head>
        <body>
            <h1>help:</h1>
            <p>
                <b>double click</b> a field to edit it<br />press <b>enter</b> to save that field<br />click <b>save</b> or press <b>ctrl+enter</b> to download your (wannabe) "csv"
            </p>
            <table>
                <?PHP foreach ($lineArr as $k => $line) { ?>
                    <?= ($k == 0 ? "<thead>" : ($k == 1 ? "<tbody>" : "")) ?>
                    <tr>
                        <?PHP foreach (explode(":", $line) as $field) { 
                            $field= trim($field);
                            ?>
                            <?= ($k == 0 ? "<th>$field</th>" : "<td>$field</td>") ?>
                        <?PHP } ?>
                        <?= ($k == 0 ? "<th>delete/add</th>" : "<td><span class='delRow'>❌</span> <span class='addRow'>✚</span></td>") ?>
                    </tr>
                    <?= ($k == 0 ? "</thead>" : ($k == count($lineArr) - 1 ? "</tbody>" : "")) ?>
                <?PHP } ?>
            </table>
            <button value="save" id="saveBtn" >save</button>
            <form method="POST">
                <input type="submit" name="reset" value="cancel">
            </form>
        <?PHP } else { ?>
        <html>
            <head>

            </head>
            <body>
                <form method="POST" enctype="multipart/form-data">
                    <input type="file" name="csv" accept=".csv" >
                    <input type="submit">
                </form>
            <?PHP } ?>
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
            <script>
                $(function () {
                    
                    bindStuff();
                });

                function saveCsv() {
                    setTimeout(function () {
                        var rows = [];
                        $("tr").each(function () {
                            var fields = []
                            $(this).find("th:not(:last),td:not(:last)").each(function () {
                                fields.push($(this).text());
                            });
                            rows.push(fields.join(":"));
                        });
                        var content = rows.join("\n");
                        var uri = 'data:application/csv;charset=UTF-8,' + encodeURIComponent(content);
                        window.open(uri);
                    }, 100);
                }
                
                function bindStuff(){
                    
                    $("#saveBtn").off().on("click", function () {
                        saveCsv();
                    });
                    
                    $(".delRow").off().on("click",function(){
                        $(this).parents("tr").remove();
                    });
                    
                    $(".addRow").off().on("click",function(){
                        var parentRow = $(this).parents("tr");
                        var newRow = $(parentRow).clone();
                        $(newRow).find("td:not(:last)").html("&nbsp;");
                        $(parentRow).after($(newRow));
                        bindStuff();
                    });

                    $("html").off().on("keydown", function (e) {
                        if (e.ctrlKey && e.key === "Enter") {
                            saveCsv();
                        }
                    });
                    
                    $("tbody > tr").each(function(){
                        var firstField = $($(this).find("td:first")[0]).text();
                        if(firstField.match("^##")){
                            $(this).addClass("fat");
                        }
                        $(this).find("td:not(:last)").each(function () {
                            $(this).off().on("dblclick", function () {
                                console.log($(this));
                                $(this).html("<input type='text' value='" + $(this).text() + "'>").off().on("keydown", function (e) {
                                    if (e.key === "Enter") {
                                        $(this).html($(this).find("input").val());
                                        bindStuff();
                                    } else if (e.key === ":") {
                                        e.preventDefault();
                                        console.error("NO COLONS HERE BRUH");
                                    }
                                });
                                var tmpVal = $(this).find("input").val();
                                $(this).find("input").focus().val('').val(tmpVal);
                            });
                        });
                        
                    });
                    
                }
            </script>
            <style>
                tbody tr:nth-child(odd){
                    background-color:#CCC;
                }
                .fat{
                    font-weight: bolder;
                }
            </style>
        </body>
    </html>
